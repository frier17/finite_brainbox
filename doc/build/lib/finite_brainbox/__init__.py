import crypt
import types
import uuid
from collections import namedtuple
from hashlib import sha256
from typing import Sequence, Any, Mapping
from datetime import datetime
from enum import Enum
from uuid import UUID
from .errors import RegistryError, TransitionStateError


Signal = namedtuple('Signal', ('phase', 'next', 'code'))


class BaseEvent:
    """
    Models an event class for sending signal across application.
    Each signal is identified via constant name or value identified as enum
    """
    __slots__ = '_name', '_cancelable', '_source', '_timestamp', \
                '_payload', '_identity', '_listeners'

    def __init__(self, identity: Any = None, name: str = None,
                 cancelable: bool = False, source: Any = None,
                 payload: Mapping = None) -> None:
        self._listeners = set()

        self._name = name if isinstance(name, str) and len(name) < 50 and name.isalnum() \
            else None
        self._cancelable = bool(cancelable) if cancelable else False
        self._source = source if isinstance(source, object) else self.__class__
        self._payload = dict(payload) if isinstance(payload, Mapping) else {}
        self._timestamp = datetime.now().timestamp()
        if isinstance(identity, (Enum, int, str, UUID)):
            self._identity = identity
        else:
            raise RuntimeError(
                'Event object should have unique identity of type (Enum, int, str, UUID)')

    def notify(self) -> None:
        for listener in self._listeners:
            listener.handle(event=self)

    def attach(self, listener) -> None:
        if not (hasattr(listener, 'handle') and
                isinstance(getattr(listener, 'handle'),
                           (types.MethodType, types.FunctionType))):
            raise RuntimeError('Invalid event listener object provided. '
                               'Expected object with handle method but got %s' %
                               type(listener))
        if listener not in self._listeners:
            self._listeners.add(listener)

    @property
    def name(self):
        return self._name

    @property
    def cancelable(self):
        return self._cancelable

    @cancelable.setter
    def cancelable(self, value):
        self._cancelable = bool(value)

    @property
    def source(self):
        return self._source

    @property
    def timestamp(self):
        return self._timestamp

    @property
    def payload(self):
        return self._payload

    @property
    def identity(self):
        return self._identity
    

class TransitionEvent(BaseEvent):
    __slots__ = '_code', '_payload', '_source'

    def __init__(self, code: int = None, payload: Mapping = None, source: Any = None):
        super(TransitionEvent, self).__init__(payload=payload, source=source)
        if isinstance(code, int):
            self._code = code

    @property
    def code(self):
        return self._code


class TransitionState:
    __slots__ = 'identity'

    def __init__(self, code):
        uid = str(uuid.uuid4())
        hashed = sha256(str.encode(uid))
        hashed.update(str.encode(crypt.mksalt(crypt.METHOD_SHA256)))
        label = hashed.hexdigest()[:12]
        identity = namedtuple('identity', ('label', 'code'))
        self.identity = identity(label, str(code))


class StateTransitionRegistry:
    _registry = ()

    @classmethod
    def initialize(cls, register: Sequence) -> None:
        try:
            if isinstance(register, Sequence):
                allowed = [(x, y, z) for x, y, z in register if
                           isinstance(y, TransitionState) and
                           isinstance(z, TransitionState) and isinstance(x, Signal)]
                if allowed:
                    cls._registry = allowed
        except Exception:
            raise RuntimeError(
                'Invalid registry provided. Expected Sequence of '
                'form (<Signal class>, <State class>, <State class> but got: \n%s' % register)

    @classmethod
    def get(cls, state, signal):
        if not cls._registry:
            raise RegistryError from RuntimeError

        next_state = [(x, y, z) for x, y, z in iter(cls._registry) if
                      x.code == signal.code and y.identity == state.identity]
        if next_state:
            return list(next_state).pop()


class Transition:
    __slots__ = 'current', 'previous', 'input_signal', 'event', 'counter', 'MAX_COUNT'

    def __init__(self, initial: TransitionState, input_signal: Signal = None,
                 max_count: int = 0, event: TransitionEvent = None) -> None:
        self.counter = 0
        self.previous = None
        if max_count is None or isinstance(max_count, int) and \
                (max_count == 0 or max_count < 0):
            self.MAX_COUNT = 0
        elif max_count > 0:
            self.MAX_COUNT = max_count
        if isinstance(initial, TransitionState):
            self.current = initial
        if isinstance(input_signal, Signal):
            self.input_signal = input_signal
        else:
            self.input_signal = None
        if isinstance(event, TransitionEvent):
            self.event = event

    def transit(self, signal: Signal, payload: Mapping = None) -> None:
        def change_state(state: Any, source: Transition):
            x, y, z = state
            source.current = z
            source.previous = y
            source.counter += 1
            source.event = TransitionEvent(signal.code, payload=payload, source=self.__class__)
            source.event.notify()

        next_state = StateTransitionRegistry.get(self.current, signal)
        if next_state:
            if self.MAX_COUNT <= 0 or self.MAX_COUNT is None:
                # allow infinite transitions
                change_state(next_state, self)
            elif self.MAX_COUNT > 0 and self.counter <= self.MAX_COUNT:
                change_state(next_state, self)
            else:
                raise TransitionStateError('Transition halted. Undetermined State '
                                           'or max recursion reached') from StopIteration
