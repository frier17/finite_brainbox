from finite_brainbox import *


class TestHandler:
    @staticmethod
    def handle():
        return NotImplemented


def test_transition():
    test_handler = TestHandler()
    test_event = TransitionEvent(identity=4757, name='TestEvent', cancelable=False,
                                 source=None, payload=None)
    test_event.attach(test_handler)

    q0 = TransitionState(4757)
    q1 = TransitionState(4891)
    q2 = TransitionState(5963)
    s1 = Signal(1, 0, 4757)
    s2 = Signal(0, 1, 4891)
    s3 = Signal(0, 1, 5963)
    service_transitions = [
        (s1, q0, q1),
        (s2, q1, q2),
        (s3, q2, q2),
    ]
    StateTransitionRegistry.initialize(service_transitions)

    t1 = Transition(initial=q0, input_signal=s1, event=test_event, max_count=-1)
    t1.transit(s1)
    t1.transit(s2)
    assert t1.event
    assert t1.event.source
    assert t1.event.cancelable is False
    assert t1.event.name
    assert t1.event.payload == {}
    assert t1.event.timestamp
    assert t1.previous == q1
    assert t1.current == q2
    assert t1.input_signal == s1
    assert t1.current.identity != q1.identity
    assert StateTransitionRegistry.get(q0, s1)
