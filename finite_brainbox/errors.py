# Define the various system Error classes


class RegistryError(ValueError):
    pass


class ApplicationError(Exception):
    pass


class TransitionStateError(ApplicationError):
    pass
