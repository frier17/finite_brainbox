import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="finite_brainbox",
    version="0.0.2",
    author="A. G. Friday",
    author_email="frier17@a17s.co.uk",
    description="A simple decision making system built from a generic finite state machine "
                "implemented in Python",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/frier17/finite_brainbox",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
