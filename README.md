# Finite Brainbox 
**A simple decision making system built from a generic finite state machine implemented in Python**

The finite state machine is a computational system that can model some basic problems. FSM can be viewed as
 classifiers, transducers, acceptors or sequencers:
+ As classifier or controller - the machine is used to classify a set of input into known set of outputs based on its
 current state
. Output in this scenario is based on state and not the input sequence.
+ As transducer - the machine acts as a black box that converts an input sequence into a determined output sequence
+ As acceptor - the FSM can accept signals or input and then moves from state from current state to another. If the
 machine is such that all accepted or valid signal changes the state to, say, q1 and all other input keeps the
  machine at state q2, then the FSM can be viewed as an acceptor with all sequence of accepted input as the
   accepted language of the machine.
+ As a sequencer or generator - the FSM which produces outputs with input signal can be made to generate a set of
 output signals by providing a fixed set of inputs. This usage makes the FSM act as a generator of desired output
  signal. An example of a sequencer is a MIDI (Musical Instrument Digital Interface) sequencer, used to drive electronic
musical instruments. The output alphabet of a MIDI sequencer is a set of 16-bit words, each having a special
 interpretation as pitch, note start and stop, amplitude, etc.
 
The FSM although very limited in the nature of problems it can solve finds very important usage in computational
 problems with simple pattern matching, example, regular expressions, sequential logic circuits common in computers
  and device controllers, and intimate relationship with directed graphs having arcs labeled with symbols from the input alphabet. 
Considering the nature of FSM as a transducer or black box whose internal state can be altered by known inputs
, complex mathematical and computational problems can be broken down into simpler state transition problems. As
 example, a chess game could be designed such that all possibilities can be computed and best path taken based on the
  current state (positions of pieces). Also a traffic light system can move state from Green, to Amber, to Red based
   on input signals such as speed of incoming vehicle. Thus, having a generic FSM system can afford developers to
    have a means by which business logic can be factored into decision logic comprising of simple transition of
     states. Adopting an event driven development approach, the change in state can be broadcast to corresponding
      event handlers. This approach can loosely couple sub systems such that very complex logic broken into simpler
       units are micro managed by collection of event handlers for predefined state changes. 

This work aims at achieving such a machine that uses the Finite State Machine to serve as a "brain box" for simple
 decision making in business applications. 
This application comprises mainly of the following:
+ `TransitionState` that defines the current state of a system
+ `Transition` that defines the possible change in states for a collection of inputs. Changes are defined by rules or
 messages to the state machine.
+ `StateTransitionRegistry` which holds the system rules that defines the allowable changes in states and the
 required signal for every combination of state change
+ `TransitionEvent` that can be broadcast to event listeners for every registered transition or change in state
+ `Signal` a message defined as tuple of three elements namely: `phase`, `next`, `code`.
+ A collection of error classes namely: `RegistryError` raised if an unknown state is being looked up in the state
 registry; `ApplicationError` the base exception for the finite state machine; `TransitionStateError` the exception
  raised when a change in state is attempted with invalid signal or unknown target state may be reached by the machine.
     
## Project status: 
Prototype

## Installation
The application can be installed by cloning the repository: 
[https://gitlab.com/frier17/finite_brainbox](https://gitlab.com/frier17/finite_brainbox). Future development may be made available  through the PiPy directory.

## Usage
Use the fbb to build application with requirement fitting the change of system or subsystems due to change in state
 or sequence of input. By having a registry of States, Signal, and collection of event handlers complex decisions can
  be reached from a centrally managed collection of rules defined as combination of Signals and States.

Example usage

~~~python
from finite_brainbox import TransitionState, StateTransitionRegistry, Transition, Signal, TransitionEvent
# Register set of rules
service_transitions = [
  (Signal(1, 0, 4757), TransitionState(4757), TransitionState(4891)),
  (Signal(0, 1, 4891), TransitionState(4891), TransitionState(5963)),
  (Signal(0, 1, 5963), TransitionState(5963), TransitionState(5963)),
]
# Initialize the registry with defined rules
StateTransitionRegistry.initialize(service_transitions)

# Monitor a service using transition object
event = TransitionEvent(4757)
event.attach(lambda: NotImplemented)
t1 = Transition(initial=TransitionState(4891), input_signal=Signal(1, 0, 4757), event=event, max_count=-1)
print('Transiting to another state')
# if the Signal is valid, t1 will change state 
# from TransitionState(4891) to TransitionState(5963) 
# for the given Signal(0, 1, 4891). 
t1.transit(Signal(0, 1, 4891))  # Each valid transition will internally fire associated event
~~~         